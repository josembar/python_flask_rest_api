from flask import Flask, request, json, jsonify
import os
app = Flask(__name__)


#books endpoint.
@app.route('/books', methods=['GET','POST'])
def books_endpoint():
    if request.method == 'GET':
        return {"allBooks": getBooksFromFile()}
    else:
        return createNewBook(request.args['id'], request.args['title'], request.args['author'])


#books endpoints that receives the book id.
@app.route('/books/<id_book>', methods=['GET','PATCH','PUT','DELETE'])
def books_endpoint_id(id_book):
    if request.method == 'GET':
        return getExistingBook(id_book)
    if request.method == 'PATCH':
        return updateExistingBook(id_book,request.args.get('title'),request.args.get('author'))
    if request.method == 'PUT':
        return createNewOrUpdateExistingBook(id_book,request.args['title'],request.args['author'])
    if request.method == 'DELETE':
        return deleteExistingBook(id_book)


#function to return the absolute path of books.txt file.
def absolutePathBooksFile():
    return str(os.path.dirname(os.path.abspath(__file__)) + '\\books.txt')


#function to check if the books.txt file exist
def doesBooksFileExists():
    return os.path.isfile(absolutePathBooksFile())


#handling 404 error
@app.errorhandler(404)
def notFound(error=None):
    message = {
      'status': 404,
      'message': 'Not Found: ' + request.url,
    }
    response = jsonify(message)
    response.status_code = 404
    return response


#function to return all books from books.txt
def getBooksFromFile():
    if doesBooksFileExists():
        with open(absolutePathBooksFile()) as books_file:
            return json.load(books_file)
    else:
        return []


#function to save a json to books.txt file
def saveBooksToFile(books):
    with open(absolutePathBooksFile(), 'w') as books_file:
        json.dump(books, books_file)


#function to create a new book
def createNewBook(id_book, title, author):
    new_book = {'id': id_book, 'title': title, 'author': author}
    if doesBooksFileExists():
        books = getBooksFromFile()
    else:
        books = []
    books.append(new_book)
    saveBooksToFile(books)
    return {"newBook": new_book}


#function to get an existing book
def getExistingBook(id_book):
    if doesBooksFileExists():
        books = getBooksFromFile()
        for key, value in enumerate(books):
            if value['id'] == id_book:
                return value
        return notFound()
    else:
        return notFound()


#function to update a value of an existing book
def updateExistingBook(id_book,title=None,author=None):
    if doesBooksFileExists():
        books = getBooksFromFile()
        for key, value in enumerate(books):
            if value['id'] == id_book:
                if title == None and author == None:
                    return {"notUpdatedBook": value}
                elif title != None and author == None:
                    value['title'] = title
                else:
                    value['author'] = author
                saveBooksToFile(books)
                return {"updatedBook": value}
        return notFound()
    else:
        return notFound()


#function to do a whole update of an existing book or create it if it's not found
def createNewOrUpdateExistingBook(id_book,title,author):
    if doesBooksFileExists():
        books = getBooksFromFile()
        found = False
        for key, value in enumerate(books):
            if value['id'] == id_book:
                found = True
                value['title'] = title
                value['author'] = author
                saveBooksToFile(books)
                return {"updatedBook": value}
        if not found:
            return createNewBook(id_book, title, author)
    else:
        return notFound()


#function to delete an existing book
def deleteExistingBook(id_book):
    if doesBooksFileExists():
        books = getBooksFromFile()
        for key, value in enumerate(books):
            if value['id'] == id_book:
                deleted_book = value
                books.pop(key)
                saveBooksToFile(books)
                return {"deletedBook": deleted_book}
        return notFound()
    else:
        return notFound()


if __name__ == "__main__":
    app.run(host ='0.0.0.0')